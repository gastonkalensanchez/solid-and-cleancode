using System.IO;
using UnityEngine;

public class FilePersistence : ILoad, ISave
{
    private const string path = "Assets/Resources/saveFile.txt";
    public float LoadLastDuration()
    {
        var reader = new StreamReader(path);
        var durationString = (reader.ReadToEnd());
        reader.Close();
        if (!string.IsNullOrEmpty(durationString))
        {
            return float.Parse(durationString);
        }
        return 0;
    }

    public void SaveLastDuration(float duration)
    {
        var writer = new StreamWriter(path,true);
        writer.WriteLine(duration);
        writer.Close(); 
    }
}
