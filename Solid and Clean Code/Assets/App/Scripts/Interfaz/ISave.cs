using UnityEngine;

public interface ISave
{
    public void SaveLastDuration(float duration);
}
