using UnityEngine;

public interface IDamage
{
    public void AddDamage(int damage);
}
