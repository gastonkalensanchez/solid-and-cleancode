using UnityEngine;

public interface IMovementPlayer 
{
    public void Mov(float pSpeed);
    public void Configure(Animator pAnimator, SpriteRenderer pSpriteRenderer, Transform pTransform);
}
