using UnityEngine;

public interface IHeal
{
    public void Heal(int heal);
}
