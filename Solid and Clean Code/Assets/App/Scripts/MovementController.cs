using System.Collections;
using Unity.Mathematics;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    
    [SerializeField] private float _speed = 1;

    private float _currentSpeed;

    IMovementPlayer _drunkMovement;
    IMovementPlayer _normalMovement;
    IMovementPlayer _currentMovement;

    private void Awake()
    {
        Reset();
    }
    public void Configure(IMovementPlayer pDrunkMovemnt,IMovementPlayer pNormalMovement)
    {
        _drunkMovement = pDrunkMovemnt;
        _drunkMovement.Configure(_animator,_spriteRenderer,transform);
        _normalMovement = pNormalMovement;
        _normalMovement.Configure(_animator, _spriteRenderer, transform);
    }

    
    private void FixedUpdate()
    {
        _currentMovement.Mov(_currentSpeed);
    }

    public void Reset()
    {
        _currentMovement = _normalMovement;
        _currentSpeed  = _speed;
        _animator.SetBool("IsDeath", false);
    }
    
    public void SetSpeed(float speed)
    {
        _currentSpeed = speed;
    }
    public void SetDrunk(float pDuration)
    {
        _currentMovement = _drunkMovement;
        StartCoroutine(DesactivarDrunk(pDuration));
    }
    public IEnumerator DesactivarDrunk(float pDuration)
    {
        yield return new WaitForSecondsRealtime(pDuration);
        _currentMovement = _normalMovement;
    }
}