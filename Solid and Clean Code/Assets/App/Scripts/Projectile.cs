﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private int _damage = 10;
    [SerializeField] private float _speed  = 3;

    private void FixedUpdate()
    {
        transform.Translate(Vector3.down * (_speed * Time.fixedDeltaTime));
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        var damage = other.collider.GetComponent<IDamage>();
        if (damage != null)
        {
            // Add damage and destroy the object
            damage.AddDamage(_damage);
            Destroy(gameObject);
        }
        else
        {
            // only destroy the object, is not a player
            Destroy(gameObject);
        }
    }
}
