﻿using UnityEngine;

public class HealItem : Item
{
    
    [SerializeField] private int _heal;

    private void OnCollisionEnter2D(Collision2D other)
    {
        var heal = other.collider.GetComponent<IHeal>();
        if (heal != null)
        {
            heal.Heal(_heal);
            Destroy(gameObject);
        }

    }
}
