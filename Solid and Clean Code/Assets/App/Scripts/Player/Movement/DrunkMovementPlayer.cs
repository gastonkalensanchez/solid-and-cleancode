using UnityEngine;
using Unity.Mathematics;
using UnityEngine.UIElements;
public class DrunkMovementPlayer : IMovementPlayer
{
    public Animator _animator;
    public SpriteRenderer _spriteRenderer;
    public Transform transform;

 
    public void Mov(float pSpeed)
    {
        // Check input
        var horizontal = Input.GetAxis("Horizontal");
        _animator.SetFloat("Horizontal", math.abs(horizontal));
        _spriteRenderer.flipX = horizontal < 0;
        var x = horizontal * pSpeed * Time.deltaTime;
        // Move according the input
        transform.Translate(-x, 0.0f, 0.0f);
    }
    public void Configure(Animator pAnimator, SpriteRenderer pSpriteRenderer, Transform pTransform)
    {
        _animator = pAnimator;
        _spriteRenderer = pSpriteRenderer;
        transform = pTransform;
    }
}
