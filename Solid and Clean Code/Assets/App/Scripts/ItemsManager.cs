﻿using System;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

public class ItemsManager : MonoBehaviour
{
    [SerializeField] private GameObject[] _spawnPoints;
    [SerializeField] private float _minSpawnInterval = 2;
    [SerializeField] private float _maxSpawnInterval = 4;
    private List<GameObject> _items = new List<GameObject>();
    private bool _canSpawn;
    private float _timeToNextSpawn;

    [SerializeField] private string[] _idItem;
    [SerializeField] private Item[] _prefabItems;
    private Dictionary<string, Item> id_Item_Prefab;
    private void Awake()
    {
        id_Item_Prefab = new Dictionary<string, Item>();
        foreach (var item in _prefabItems)
        {
            id_Item_Prefab.Add(item.Id, item);
        }
    }
    private void Start()
    {
        CalculateTimeToNextSpawn();
    }
    private void Update()
    {
        if (_canSpawn)
        {
            _timeToNextSpawn -= Time.deltaTime;
            if (_timeToNextSpawn < 0)
            {
                CalculateTimeToNextSpawn();
                SpawnItem(_idItem[Random.Range(0,_idItem.Length)]);
            }
        }
    }

    private void SpawnItem(string pID)
    {
        if (!id_Item_Prefab.TryGetValue(pID, out var itemInstanciado))
        {
            throw new ArgumentOutOfRangeException();
        }
        _items.Add(Instantiate(itemInstanciado, _spawnPoints[Random.Range(0, _spawnPoints.Length - 1)].transform.position, Quaternion.identity).gameObject);
    }
    private void CalculateTimeToNextSpawn()
    {
        _timeToNextSpawn = Random.Range(_minSpawnInterval, _maxSpawnInterval);
    }


    
    public void DestroyItems()
    {
        foreach (var projectile in _items)
        {
            Destroy(projectile);
        }

        _canSpawn = false;
    }
    
    public void StartSpawning()
    {
        _canSpawn = true;
    }
}
