using System;
using UnityEngine;

public class Installer : MonoBehaviour
{
    [SerializeField] private Menu menu;
    [SerializeField] private GameManager gameManager;
    private void Awake()
    {
        menu.Configure(GetLoadPersistence());
        gameManager.Configure(GetSavePersistence());
    }

    private ISave GetSavePersistence()
    {
#if UNITY_EDITOR
        return new FilePersistence();
#endif
        return new Persistence();
    }

    private ILoad GetLoadPersistence()
    {
#if UNITY_EDITOR
        return new FilePersistence();
#endif
        return new Persistence();
    }
}
